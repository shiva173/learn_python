PASSWORDS = {
    'email': 'dsadsadasdasdgfdgADSA32',
    'blog': 'dsadsadvbcbfg$#$3424',
    'luggage': '12345'
}

import sys, pyperclip
if len(sys.argv) < 2:
    print('usage: python pw.py [username]')
    sys.exit()
account = sys.argv[1]

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('Password for ' + account + 'copy to buffer.')
else:
    print('Account ' + account + "dos'nt exists.")

